# Adaptive Reduced Tempering
Written By Patrick Héas.                                          
Copyright 2024 INRIA.
GNU Affero General Public License, AGPL-3.0-or-later

## Description

This code implements the algorithm presented in the paper  `Adaptive reduced tempering For Bayesian inverse problems and rare event simulation' by F. Cérou, P. Héas and M. Rousset. By the use of reduced order models, this algorithm lowers the computational cost of Bayesian sampling using adaptive SMC simulation based on reduced models. The sampling algorithm is used to accelerate rare event simulation and solving Bayesian inverse problems.  

The code is written in the Julia language: https://docs.julialang.org

The two demo provided here correspond to the evaluation benchmark detailed in the article referenced above. 

-(A) ART (Adaptive reduced tempering): 
       
this first demo implements rare event simulation,
where the score function is the L_p norm of a parametric PDE solution approximated by reduced basis. 
The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
The PDE is parametrized by a random vector of diffusion coefficients.
The reference distribution for the random vector components are independent log-normal distributions.

-(B) BART (Bayesian adaptive reduced tempering): 
       
this second demo implements Bayesian sampling of some posterior distribution,
in the context of an inverse problem. More precisely, the code samples 
the posterior distribution of the supremum norm of a parametric PDE solution with random inputs, 
given a set of noisy point-wise observations of the PDE solution on the the domain.
The PDE solution is approximated by reduced basis.
The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
This PDE is parametrized by a random vector of diffusion coefficients. 
The posterior is built given observations following a Gaussian likelihood model
and using independent log-normal prior distributions for the diffusion coefficients.

## Dependencies: Python, pyMor library and Julia packages

ART requires the Julia packages appearing in the Project.toml file. They can be installed typing in a Julia terminal: 

    ]
    instantiate
 

In this demo, ART requires the installation of Python programming language https://www.python.org/, as it uses routines of the pymor libraires https://github.com/pymor/pymor for solving paramteric PDEs and reduced modeling with reduced basis techniques. The python routines are interfaced with julia thanks to the pyCall Julia package.


## Run demos
   
Create the directory for results

    mkdir test

A) Rare event simulation, 
         
launch in a Julia REPL terminal (default input parameters can be modified at the beginning of the file)

    art_runRBdemo.jl

    

ART's estimates and its cost are saved in the julia binary file 

'test/art_RBdemo_outputs.jld' 

The visualization of ART's IS and RSMC estimates over the iterations of the algorithm, as well as the graphs of MSE versus cost, are saved in the files 

'test/art_RBdemo_mean_wrt_iteration.pdf' 

and
    
'test/art_RBdemo_RMSEvsCost_wrt_iteration.pdf' 

B) Posterior sampling in the context of a PDE-based inverse model with RB approximations

launch in a Julia REPL terminal (default input parameters can be modified at the beginning of the file)
    
    bart_runRBdemo.jl
    

BART's output posterior distribution estimates are stored in the binary file 

'test/bart_RBdemo_outputs.jld' 

Samples and weights can be loaded using for IS estimate the command (in a Julia REPL terminal) 

    load("test/bart_RBdemo_outputs")["samples_SinfIS"]
    load("test/bart_RBdemo_outputs")["weights_SinfIS"]

and for RSMC estimate the command 
    
    load("test/bart_RBdemo_outputs")["samples_SinfSMC"]
    load("test/bart_RBdemo_outputs")["weights_SinfSMC"]
    
A true posterior distribution (computed using (non-reduced) adaptive tempering) is provided in the file 

'test/bart_RBdemo_refDistribution.jld' 


Plots of ART's IS and RSMC posterior distribution histograms are saved in the file 

'test/bart_RBdemo_posteriorAndRef_histograms.pdf' 

or 

'test/bart_RBdemo_posterior_histograms.pdf'
    
depending the true posterior is provided as input or not.

## Code  brief description

-The main of ART and BART codes are in 'art_main_algorithm.jl' and 'bart_main_algorithm.jl'

-The model (evaluation of the score / reduced score model and error, sampling procedure for the reference distribution, etc) are coded in 'art_model_and_parameters.jl' and 'bart_model_and_parameters.jl'

-The subroutines for increasing level until crtical one and bridging are in 'art_smc_algorithms.jl'

-The routines for display and saving are in 'art_plots.jl'

-The pymor library is imported via conda in "art_librairies.jl"


