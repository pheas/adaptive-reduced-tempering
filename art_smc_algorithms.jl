#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

function SMC_increase_beta!(rom_current,samples,w,Z,M,beta,logCostLevel,param) 

    maxIt=2000

    # Rom_current samples scores and errors
    sRom,eRom,eRomNoTreshold=evaluateROMandError(rom_current,samples)
    
    #Initialize loop 
    i=1
    logcosttry=0
    betatry=0

    alphaPropSequence=[param.kernelParam.alpha_prop]
    Minter=[0.]

    #Loop
    while (logcosttry<=0) &&  (i<=maxIt)  && (betatry<param.beta_max)
    
        #increase beta to match quantile level
        betatry=nextbeta(beta[i],w,sRom,param) 
        #importance sampling log cost budget difference for betatry
        if (logCostLevel<Inf64)
            logcosttry=worstlogcostconstraint(betatry,beta[i],w,sRom,eRom,sRom,logCostLevel)
        else
            logcosttry=-1*worstlogcostconstraint(betatry,beta[i],w,sRom,eRom,sRom,0)
        end
        if ( logcosttry<=0 &&  betatry<=param.beta_max && beta[i]<param.beta_max)
            
            #accept beta increase
            push!(beta,betatry)
            #compute weights
            push!(Minter,maximum( (betatry-beta[i]).*sRom ))
            w.=w.* exp.( (betatry-beta[i]).*sRom .-Minter[end])
            alpha_=sum(w)
            w.=w./alpha_
            #update nomalization
            push!(Z,Z[i]*alpha_)
            #resample mutate 
            resampleMutate!(rom_current,samples,w,beta[i+1],sRom,param)
            push!(alphaPropSequence,param.kernelParam.alpha_prop)
            # update scores and errors
            sRom,eRom,eRomNoTreshold=evaluateROMandError(rom_current,samples)
            i+=1
            println("--------------> beta ",betatry, ", diff WLC=",logcosttry ,", nb eval ROM/FOM ", rom_current.nbROMcalls,"/",rom_current.fomTbp[].nbFOMcalls,", nb samples in surrogate rare event ",sum(Float64.(sRom .>=1)));
        end
    end

    println("beta^(k)=",beta[end]," Z^(k)=",Z[end],", (betatry=",betatry," diff WLC=",logcosttry,", nb eval ROM ", rom_current.nbROMcalls," nb beta increase=", i-1,")") 
    
    RomConvergence=false
    # to stop ROM updating if ROM is accurate :
    #1) log cost is very small (and log cost treshold set to infinite), 2) beta ~ betamax, and 3) (only for rare event simulation) large proportion smaples in rare event 
    if abs(logcosttry)<=param.logCost && logCostLevel==Inf64 &&  abs(beta[end]-param.beta_max) < 1e-2 &&  mean(sRom) >= 1-param.epsilon
        RomConvergence=true
    end
    push!(M,sum(Minter))

    return sRom,eRom,RomConvergence,eRomNoTreshold

end

function bridging!(Z,M,beta,w,samplesHistory,samples,sRom,eRom,it,roms,logCostLevel,maxHistory,param)

    # -> search tilde k and level l_0(tilde k) satisfying quantile condition   
    kprim=it+1
    betaprim=-Inf64
    sRom_kprim=sRom
    eRom_kprim=eRom
    sRom_next=copy(sRom) # arbitrary for initialization 
    eRom_next=copy(eRom) 
    while ( betaprim < 0 && kprim>1 && it-kprim<maxHistory)
        kprim=kprim-1

        # previous and next reduced scores and errors using previous N-sample
        if (kprim<it)
             # corresponds to past scores and errors
             sRom_kprim,eRom_kprim=evaluateROMandError(roms[kprim],samplesHistory[kprim])
        end
        sRom_next,eRom_next=evaluateROMandError(roms[it+1],samplesHistory[kprim])

        # find for kprim the maximum inverse temperature meeting the entropic conditions (if it exists)
        betaprim=find_max_feasible_beta(beta[kprim][end],w[kprim],sRom_next,eRom_next,sRom_kprim,1-param.proportionKilling,logCostLevel,param)
        if betaprim < 0 println(" no feasible point for rom ",kprim) end
    end
    println("kprim=",kprim-1, ", betaprim= ",betaprim) 
    
    if (betaprim < 0 ) 
         println("no distribution to bridge with next ROM in the previous ones ... ") 
    else
         println("previous rom index for bridging ",kprim-1, ", nb of steps back ",it-kprim, ", beta0(kprim)=",betaprim) 
    end

    # -> compute the normalization correction, or prepare to restart SMC simulation
    if (kprim>1 && it-kprim<maxHistory) 
         samples.=samplesHistory[kprim]
         beta[it+1][end]=betaprim
         #compute weights
         Mbridge=maximum(betaprim.*sRom_next .- beta[kprim][end].*sRom_kprim)
         w[it+1].=w[kprim].* exp.( betaprim.*sRom_next .- beta[kprim][end].*sRom_kprim .- Mbridge)
         alpha_=sum(w[it+1])
         w[it+1].=w[it+1]./alpha_
         #update nomalization
         Z[it+1][end]=alpha_*Z[kprim][end]
         #println("Mbridge= ",Mbridge," sum(M[kprim])= ",sum(M[kprim]), " M[it+1]=",sum(M[kprim])+Mbridge)
         push!(M,[sum(M[kprim])+Mbridge])
         println("correction -> Z0(kprim): ",Z[it+1][end],"=",Z[kprim][end],"x",alpha_," , beta0(kprim)=",betaprim, " (kprim=",kprim-1,")") 
         #resample mutate
         resampleMutate!(roms[it+1],samples,w[it+1],beta[it+1][end],sRom_next,param)
     else # else re-intialize and prepare to restart simulation  from beginning
         println("restart beginning -> Z=1") 
         w[it+1].=copy(w[1])
         Z[it+1][end]=1.
         push!(M,[0.])
         beta[it+1][end]=0
         samples.=refSampler(param.refLaw,param.N_samples,param.rng)
         param.kernelParam.alpha_prop=param.alphaProp_init
     end
end


function crossentropyconstraint(beta,beta0,w,S,S0,alpha)
    phi=beta.*S.-beta0.*S0 
    gamma=sum( w.* exp.( phi ) .* phi ) / sum( w.* exp.( phi ) )
    res=log(alpha)-log(sum( w.* exp.( phi ) )) + gamma
     if isnan(res) || isinf(res) 
         res=1e2
     end
    return res   
end


function worstlogcostconstraint(beta,beta0,w,S,E,S0,alpha)
    phi=beta.*S.-beta0.*S0 
    phichech=beta.*(S .- E) .- beta0.*S0 
    meanexpphichech=sum(w.*exp.(phichech))
    res=log(sum(w.*exp.(phi))/meanexpphichech)-beta*sum(w.*exp.(phichech).*E)/meanexpphichech
    if isnan(res) || isinf(res)
        #println("invalid point WLC:", res," / ",beta)
        res=1e2
    end
    return res-alpha
end


function nextbeta(beta0,w,S,param)
    alpha=1-param.proportionKilling
    f(x)=crossentropyconstraint(x,beta0,w,S,S,alpha)
    factor=1.
    fmax=f(param.beta_max*factor)
    while isnan(fmax) || isinf(fmax) 
        factor=factor/2.
        fmax=f(param.beta_max*factor)
    end
    root=find_zeros(f,beta0,param.beta_max*factor)
    if size(root,1)==0
        tmp=crossentropyconstraint(param.beta_max*factor,beta0,w,S,S,alpha)
        if tmp<0
            return param.beta_max*factor
        else
            return beta0
        end
    else
        return min(maximum(root),param.beta_max)    
    end
end



function find_max_feasible_beta(beta0,w,S,E,S0,alpha,logCostLevel,param)
        
    model = Model(Ipopt.Optimizer)
    set_silent(model)
    @variable(model,min(param.beta_max,beta0*2) >= x >= 0.)
    @NLobjective(model, Max, x)
    g1(x)=crossentropyconstraint(x,beta0,w,S,S0,alpha)
    register(model, :g1, 1, g1; autodiff = true)
    @NLconstraint(model, c1, g1(x)<=0)
    g2(x)= worstlogcostconstraint(x,beta0,w,S,E,S0,logCostLevel)
    if logCostLevel<Inf64
        register(model, :g2, 1, g2; autodiff = true)
        @NLconstraint(model, c2, g2(x)<=0)
    end
    JuMP.optimize!(model)
    if string(termination_status(model))=="OPTIMAL" || string(termination_status(model))=="LOCALLY_SOLVED" 
        println("** optimal solution = ", JuMP.value(x))
        return JuMP.value(x)
    elseif string(JuMP.ResultStatusCode)=="FEASIBLE_POINT"
        println("** feasible solution = ", JuMP.value(x))
        return JuMP.value(x)
    elseif g1(beta0)<=0 && ( logCostLevel==Inf64 || g2(beta0)<=0 )
        println("** feasible intial point = ", beta0)
        return beta0
    else
        print("** ", string(termination_status(model))," ?? ", g1(beta0), ", ", g2(beta0))
        return -Inf64
    end
end


function resampleMutate!(rom_current,samples,w,beta,S,param) 
         
    m=size(samples,1)
    dim=size(samples,2)

    #Resampling with comb 
    offsprings = f_resample_comb_randshift(w,param.rng);
    indices,mprim =f_resample_indices_part(offsprings)

    w .= ones(m)./m

    if mprim>0
    samples.=samples[indices,:]
    S .= S[indices];


    prop=ones(m,dim)
    S_prop=ones(m)
    deltaU=ones(m)
    metro=ones(m)

    #Metropolis kernel mutation
    for imove=1:param.kernelParam.nMove
        
        if param.refLaw.name=="logNormalLaw"
            # change to std Normal coordinates:
            prop .= (log.(samples).-param.refLaw.mu_ref) ./ param.refLaw.sigma_ref ;
        end
        # state move Orstein-Uhlenbeck:
        prop .= 1. ./sqrt(1. .+ param.kernelParam.alpha_prop^2) .* ( prop .+ param.kernelParam.alpha_prop .* randn(param.rng,Float64,(m,param.refLaw.paramDim)));
       
        if param.refLaw.name=="logNormalLaw"
            # back to log Normal:
            prop .= exp.(prop .* param.refLaw.sigma_ref .+ param.refLaw.mu_ref);
        end
        S_prop .=evaluateROM(rom_current,prop)

        # accept or reject
        r = rand(param.rng,m,1);
        deltaU .= exp.(beta.*(S_prop.-S));
        metro .= r.>deltaU;
    
        samples .= (1 .-metro).* prop +  metro.* samples;
        S .= (1 .-metro).* S_prop +  metro.* S;
        
        #adpat Orstein-Uhlenbeck kernel to acceptance rate 
        # [Garthwaite, Paul H., Yanan Fan, and Scott A. Sisson.
        #  Adaptive optimal scaling of Metropolis–Hastings algorithms using the Robbins–Monro process." 
        #  Communications in Statistics-Theory and Methods 45.17 (2016): 5098-5111.]  

        if refLaw.name=="logNormalLaw"   
            param.kernelParam.alpha_prop=exp(log(param.kernelParam.alpha_prop)+ param.kernelParam.c*(mean(min.(1,deltaU))-param.kernelParam.pstar)/Float64(imove))
        else
            param.kernelParam.alpha_prop=param.kernelParam.alpha_prop+ param.kernelParam.c*(mean(min.(1,deltaU))-param.kernelParam.pstar)/Float64(imove)          
        end
        #println(1-mean(metro),",",mean(min.(1,deltaU)),",",kernelParam.alpha_prop);  
              
    end
    end
end

function mutate!(rom_current,samples,beta,param)
            
      
    m=size(samples,1)
    dim=size(samples,2)
    S=evaluateROM(rom_current,samples)
    prop=ones(m,dim)
    S_prop=ones(m)
    deltaU=ones(m)
    metro=ones(m)

    #Metropolis kernel application (no kernel adaptation)
     for imove=1:param.kernelParam.nMove
        
        if param.refLaw.name=="logNormalLaw"
            # change to std Normal coordinates:
            prop .= (log.(samples).-param.refLaw.mu_ref) ./ param.refLaw.sigma_ref ;
        end
        # state move Orstein-Uhlenbeck:
        prop .= 1. ./sqrt(1. .+ param.kernelParam.alpha_prop^2) .* ( prop .+ param.kernelParam.alpha_prop .* randn(param.rng,Float64,(m,param.refLaw.paramDim)));
       
        if param.refLaw.name=="logNormalLaw"
            # back to log Normal:
            prop .= exp.(prop .* param.refLaw.sigma_ref .+ param.refLaw.mu_ref);
        end
        S_prop .=evaluateROM(rom_current,prop)

        # accept or reject
        r = rand(param.rng,m,1);
        deltaU .= exp.(beta.*(S_prop.-S));
        metro .= r.>deltaU;
    
        samples .= (1 .-metro).* prop +  metro.* samples;
        S .= (1 .-metro).* S_prop +  metro.* S;

       end

end

function f_resample_comb_randshift(w,rng)
    # the "comb" algo with a random shift (this procedure is correct !)

      n_weights      = length(w)
      p              = Permutation(randperm(rng, n_weights))
      weights       = w[p.data]
      weights=cumsum(weights)
      v=collect(0:n_weights-1)./n_weights.+rand(rng,1)/n_weights
      offsprings=zeros(n_weights)
      idx=1
      for j=1:n_weights
        while weights[idx]<v[j]
            idx=idx+1
            if idx > n_weights
                idx=1
            end
        end
        offsprings[idx]=offsprings[idx]+1
      end
      return Int64.(offsprings[inv(p).data])

end

function  f_resample_indices_part(ftable)
    # INPUT a table [i1 i2 ... in], ij : means that point #j has been drawn ij times
    # OUTPUT  rearanngement of [1*ones(1,i1) 2*ones(1,i2) ... n*ones(1;in)] with clones in front
    # ex.   mult([0   2   0   2   1   1])  gives  [2, 4, 2, 4, 5, 6], 2
    m=size(ftable,1)
    preservedoffsprings=Int64.(ftable.>=1)
    resampledoffsprings=ftable.-preservedoffsprings
    index_tomute=[]
    index_tomaintain=[]
    for j=1:m  
      if preservedoffsprings[j]>0 
        push!(index_tomaintain,j)
        while resampledoffsprings[j]>0 
            resampledoffsprings[j]=resampledoffsprings[j]-1
            push!(index_tomute,j)
        end
      end
    end
    return [index_tomute;index_tomaintain],size(index_tomute,1)
end

