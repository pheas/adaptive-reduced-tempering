#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

# Main algorithm:  Bayesian adaptive reduced tempering (BART)

function BART_bayes(romInit,param)

    N_samples=param.N_samples
    budget=param.budget
    logCost=param.logCost
    miniNumberHitsForIS=param.miniNumberHitsForIS

    #############
    #Initialization
    #############
     
    samples=refSampler(param.refLaw,N_samples,param.rng)
    Z=[[1.]]
    M=[[0.]]
    push!(M[1],0.)
    beta=[[0.]]
    w=[ones(N_samples)./N_samples]
    roms=[]
    samplesHistory=[copy(samples)]
    push!(Z,[1.])
    push!(M,[0.])
    push!(beta,[0.])
    push!(w,w[1])
    param.kernelParam.alpha_prop=param.alphaProp_init
    push!(roms,romInit); push!(roms,deepcopy(romInit))
    maxSampling=true
    NumberHits=0

    FirstIndexForIS=-1
    IS_estimates_pointwise_den=zeros(budget)
    IS_SInf=zeros(budget)
    SMC_distribution_num=zeros(budget,N_samples)
    SMC_SInf=zeros(budget,N_samples)

    nbROMeval=zeros(budget)
    nbFOMeval=zeros(budget)
    param.fom.nbFOMcalls=0
    
    logCostLevel=logCost
    continueROMUpdate=true
    ROMconvergence=0

    ##### untill the snapshot budget is reached or ROM sufficiently accurate ##### 
    it=1
    maxNumberRomConvergenceCriterion=0
    maxHistory=10
   
    while (it<budget) &&  (continueROMUpdate)
        it=it+1
        println("#################### BART iteration ",it-1," ####################")
        if ROMconvergence>maxNumberRomConvergenceCriterion  # to exit while loop at the next iteration 
            continueROMUpdate=false
        end 
        # adaptive increase of levels (cte normalization factor & entropic criterion)
        vRom,eRom,conv,eRomNoTreshold=SMC_increase_beta!(roms[it],samples,w[it],Z[it],M[it],beta[it],logCostLevel,param)
        if (conv) ROMconvergence=ROMconvergence+1 end
        push!(samplesHistory,copy(samples))
        push!(w,w[it])
        push!(beta,[beta[it][end]])
        push!(Z,[Z[it][end]])
        println("Z/beta=",Z[it][end],"/",beta[it][end]);
      
       # sampling with learning function (multinomial sampling & kernel mixing or max error)
        if (!maxSampling)
            items = [i for i in 1:N_samples]'
            weights = Weights(w[it])
            index=wsample(param.rng,items, weights,1)
            snapshotSample=samples[index,:]
            mutate!(roms[it],snapshotSample,beta[it][end],param)
        else
            snapshotSample=samples[sortperm(eRomNoTreshold)[N_samples],:]';
        end
        
       # updating the ROM 
       push!(roms,deepcopy(roms[it]))
       if continueROMUpdate 
            updateROM!(roms[it+1],snapshotSample)
       else
            println("-------------------------------------------- end of ROM updates ...")
       end
       if (it-1-max(maxHistory,maxHistory)>0) # releaving unecessary memory 
            roms[it-1-max(maxHistory,maxHistory)]=RBApprox() 
            GC.gc(); 
       end

       # learning function update & setting first index for IS estimator 
       if (beta[it+1][end]>=0.95)
            NumberHits+=1
            println("Number of times nearly unit temperature (below 1/0.95)=",NumberHits)
            if (NumberHits> miniNumberHitsForIS && FirstIndexForIS==-1)
                println("-------------------------------------------- swithcing learning function to random sampling ... ")
                maxSampling=false
                FirstIndexForIS=it  
            end
            if (!maxSampling && logCostLevel <Inf64 )
                logCostLevel=Inf64
                println("-------------------------------------------- end of entropic criterion ...  ")
            end
       end 

        # --SMC  estimate (for distribution of sInf) 
        vRom,sRomInf=evaluateROMandSROM(roms[it],samples) 
        roms[it].nbROMcalls-=N_samples # because already computed in SMC_increase_beta!
        tmpWeights=w[it+1].*exp.((1-beta[it+1][end]).*vRom.+sum(M[it]))
        SMC_distribution_num[it,:]=Z[it+1][end]*tmpWeights 
        SMC_SInf[it,:]=sRomInf
        vTmp,sTmp=evaluateFOM(param.fom,snapshotSample)  
        vFomSnapshot,sFomSnapshotInf=vTmp[1],sTmp[1]
        if continueROMUpdate 
            param.fom.nbFOMcalls-=1 # because already computed in updateROM!
        end
        vRomSnapshot=evaluateROM(roms[it],snapshotSample)[1]
        IS_estimates_pointwise_den[it]=Z[it+1][end]*exp(vFomSnapshot-beta[it+1][end]*vRomSnapshot+sum(M[it]))
        IS_SInf[it]=sFomSnapshotInf
        
           
       # bridge SMCs or decide to restart from the beginning the next SMC simulation 
       if bridge
        if continueROMUpdate
            # guaranteeing entropic criteria
            bridging!(Z,M,beta,w,samplesHistory,samples,vRom,eRom,it,roms,logCostLevel,maxHistory,param)
        end
       else
        if continueROMUpdate 
            # restart simulation 
            samples.=refSampler(refLaw,N_samples,param.rng)
            push!(M,[0.])
            w[it+1]=w[1]
            Z[it+1][end]=1.
            beta[it+1][end]=0.
            param.kernelParam.alpha_prop=param.alphaProp_init
        end
       end

       nbROMeval[it]=roms[it].nbROMcalls
       nbFOMeval[it]=roms[it].fomTbp[].nbFOMcalls
       println("nb rom/fom eval=",nbROMeval[it]," / ",nbFOMeval[it])

    end
    #####  IS for remaining snapshot budget  ##### 
    for itLeft=it+1:budget

        snapshotSample=samples[ceil.(Int64,rand(param.rng,1)*N_samples),:]
        vTmp,sTmp=evaluateFOM(param.fom,snapshotSample)  
        vFomSnapshot,sFomSnapshotInf=vTmp[1],sTmp[1]
        vTmp,sTmp=evaluateROMandSROM(roms[it],snapshotSample)
        vRomSnapshot,sRomSnapshotInf=vTmp[1],sTmp[1]
        roms[it].nbROMcalls-=1  # because already computed in SMC_increase_beta! or bridging!
        IS_estimates_pointwise_den[itLeft]=Z[it+1][end]*exp(vFomSnapshot-beta[it+1][end]*vRomSnapshot+sum(M[it]))
        IS_SInf[itLeft]=sFomSnapshotInf
        nbROMeval[itLeft]=roms[it].nbROMcalls
        nbFOMeval[itLeft]=roms[it].fomTbp[].nbFOMcalls
    
    end  
    
    #####  Computation of Sinf distributions for IS and RSMC  #####  
    IS_SinfSamples=IS_SInf[FirstIndexForIS+1:end]
    IS_SinfSamplesWeights=IS_estimates_pointwise_den[FirstIndexForIS+1:end]./sum(IS_estimates_pointwise_den[FirstIndexForIS+1:end])
    SMC_SInfSamples=vec(SMC_SInf[FirstIndexForIS+1:it,:])
    SMC_SInfSamplesWeights=vec(SMC_distribution_num[FirstIndexForIS+1:it,:])./sum(SMC_distribution_num[FirstIndexForIS+1:it,:])
    
    println("***** End run:  nbROMeval = ",nbROMeval[end], "  nbFOMeval = ",nbFOMeval[end], " *****")
    
    return    nbROMeval, nbFOMeval,IS_SinfSamples,IS_SinfSamplesWeights,SMC_SInfSamples,SMC_SInfSamplesWeights

end     