#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved    

#####################################################################################
#####################################################################################
#       This demo implements a Bayesian sampling of some posterior distribution,
#       in the context of an inverse problem. More precisely, the code samples 
#       the posterior distribution of the supremum norm of a parametric PDE solution with random inputs, 
#       given a set of noisy point-wise observations of the PDE solution on the the domain.
#       The PDE solution is approximated by reduced basis.
#       The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
#       This PDE is parametrized by a random vector of diffusion coefficients. 
#       The posterior is built given observations following a Gaussian likelihood model
#       and using independent log-normal prior distributions for the diffusion coefficients.
#       Details on the model can be found in in the paper  
#      `Adaptive reduced tempering For Bayesian inverse problems and rare event simulation' by F. Cérou, P. Héas and M. Rousset.   
#####################################################################################
#####################################################################################

include("art_librairies.jl")
include("bart_main_algorithm.jl")
include("bart_models_and_parameters.jl")
include("art_smc_algorithms.jl")
    
println("\n************************************************************************************************************** ")
println("*********************** Posterior sampling by Bayesian adaptive reduced tempering demo ************************ ")
println("****************************************************************************************************************\n ")

#####################################################################################
#----------------------------- Parameter setting -----------------------------------#
#####################################################################################

#################
## output path
#################
refpath="./test/bart_RBdemo"
rootpath="./test/"
extpath="bart_RBdemo"

#################
## BART parameters
#################

# true if bridging ART simulations
bridge=true
# budget K 
budget=200
# number of snapshots to build initial reduced score
N_samplesInit=5
# number of particles N
N_samples=200
# theta (=1-exp(Ent(mu_new|mu_current)))
proportionKilling=0.01
#worst logcost threshold 
logCost=0.001 
#number of hits before Importance Sampling trigerred  
miniNumberHitsForIS=2
#Markovian kernel 
alphaProp_init=0.3
kernelParam=OrsteinUhlenbeckParams(alphaProp_init,0.5,0.015,30)
#Maximum inverse temperature (unit value for Bayesian inverse problems)
beta_max=1

#################
## PDE and RB parameters 
#################
nbBlocksX=2
nbBlocksY=2
diameter=0.02
dimParameter=nbBlocksX*nbBlocksY

#################
# ref distribution parameters
#################
seed=1234
rng = MersenneTwister(seed)
refLaw=logNormal(dimParameter,0.6,0.8)

#################
## observations parameter
#################
nbObsPoint=20
varObs=1.e-2
noiseFreeObsParameter=[0.47016111043605696 0.4197112832011546 0.33887894454682294 0.0063881588577963856]

#################
# true posterior for evaluation
#################
# true distribution to compare to (if false save the current posterior distribution as the true one)
refFOM=true

#####################################################################################
#----------------------------- Initialization --------------------------------------#
#####################################################################################

println("\n*********************** Initialization ***********************\n ")
println("---------> Generation of observations ...")
tmpFOM=@suppress begin PDE_tbp(nbBlocksX,nbBlocksY,diameter,[],[],0.) end
obs_point=Int64.(floor.(rand(rng,nbObsPoint)*tmpFOM.sizeSolution))
tmpFOM.obs_point=obs_point
solPDEobs,S_max=evaluateSFOM(tmpFOM,noiseFreeObsParameter)
solPDEobs=vec(solPDEobs)
obs=vec(solPDEobs).+sqrt(varObs).*randn(rng,nbObsPoint)
println("obs_point=",obs_point)
println("sol_point=",vec(solPDEobs))
println("obs=",obs)
println("maxSol=",S_max)

println("---------> Building thermal block FOM ...")
fom=@suppress begin  PDE_tbp(nbBlocksX,nbBlocksY,diameter,obs_point,obs,varObs) end
println("--FOM system of dimension = ",fom.sizeSolution)

println("---------> Building thermal block initial RB ...")
initSamples=refSampler(refLaw,N_samplesInit*10,rng) 
rom_inital=@suppress begin RBApprox(fom,initSamples,N_samplesInit) end

#####################################################################################
#------Posterior sampling by Bayesian adaptive reduced tempering (BART)-------------#
#####################################################################################

println("\n*********************** Running BART ***********************\n ")
#creating BART param object and saving it
param=BART_parameters(fom,bridge,budget,N_samples,proportionKilling,logCost,miniNumberHitsForIS,refLaw,alphaProp_init,kernelParam,beta_max,rng)
println("---------> Saving BART input parameters to "*rootpath*extpath*"_parameters.txt")
saveInputParameters(rootpath*extpath*"_parameters.txt",param,rom_inital)

#running BART 
println("---------> Lauching BART ...")
timing = @elapsed nbROMcalls, nbFOMcalls,samples_SinfIS,weights_SinfIS,samples_SinfSMC,weights_SinfSMC=BART_bayes(rom_inital,param)

#saving BART outputs
println("---------> Saving BART outputs to "*rootpath*extpath*"_outputs.jld")
save(rootpath*extpath*"_outputs.jld", "nbROMcalls", nbROMcalls, "nbFOMcalls", nbFOMcalls, "timing", timing,
    "samples_SinfIS",samples_SinfIS,"weights_SinfIS",weights_SinfIS,"samples_SinfSMC",samples_SinfSMC,"weights_SinfSMC",weights_SinfSMC)

#if no true distribution is provided, saving BART RSMC posterior distribution as true posterior (for future use)
if !refFOM
    println("---------> Saving posterior distribution as the true one  ...")
    save(rootpath*extpath*"_refDistribution.jld", "RefDistribution", samples_SinfSMC, "RefDistributionWeights", weights_SinfSMC)
end

#####################################################################################
#----------------------------- Evaluation and result display   ---------------------#
#####################################################################################

println("\n*********************** Evaluation and result display ***********************\n ")
RBgain=4e-2
println("---------> BART cost (in FOM units) / cpu time (in sec.) = ",nbROMcalls[end]*RBgain+nbFOMcalls[end], " / ",timing)

if  !refFOM  
    # save histograms of IS and RSMC based posteriors
    minHistogram=minimum(samples_SinfSMC)
    MaxHistogram=maximum(samples_SinfSMC)+1e-3
    binHistogram=(MaxHistogram-minHistogram)/50
    binCollection=collect(minHistogram:binHistogram:MaxHistogram)
    IS_SinfHistogram=normalize(fit(Histogram, samples_SinfIS, Weights(weights_SinfIS),binCollection ),mode=:probability)
    SMC_SinfHistogram=normalize(fit(Histogram, samples_SinfSMC, Weights(weights_SinfSMC), binCollection),mode=:probability)
    p00=plot(SMC_SinfHistogram, label="RSMC posterior")
    p00=plot!(IS_SinfHistogram, label="IS posterior")
    savefig(p00,rootpath*extpath*"_posterior_histograms.pdf") 
else
    #loading true posterior distribution (if provided) 
    RefDistribution=[]
    RefDistributionWeights=[]
    println("---------> Loading true distribution for evaluation  ...")
    RefDistribution=load(refpath*"_refDistribution.jld")["RefDistribution"][end]
    RefDistributionWeights=load(refpath*"_refDistribution.jld")["RefDistributionWeights"][end]

    #save the histogram of true posterior and histograms of IS and RSMC based posteriors
    minHistogram=minimum(RefDistribution)
    MaxHistogram=maximum(RefDistribution)+1e-3
    binHistogram=(MaxHistogram-minHistogram)/50
    binCollection=collect(minHistogram:binHistogram:MaxHistogram)
    Ref_SinfHistogram=normalize(fit(Histogram, RefDistribution, Weights(RefDistributionWeights./sum(RefDistributionWeights)),binCollection ),mode=:probability)
    IS_SinfHistogram=normalize(fit(Histogram, samples_SinfIS, Weights(weights_SinfIS),binCollection ),mode=:probability)
    SMC_SinfHistogram=normalize(fit(Histogram, samples_SinfSMC, Weights(weights_SinfSMC), binCollection),mode=:probability)
    p00=plot(SMC_SinfHistogram, label="RSMC posterior")
    p00=plot!(IS_SinfHistogram, label="IS posterior")
    p00=plot!(Ref_SinfHistogram, label="true posterior")
    savefig(p00,rootpath*extpath*"_posteriorAndRef_histograms.pdf") 

    #evaluation of Kolmogorov-Smirnov distance 
    KolmogorovSmirnovIS=-1
    KolmogorovSmirnovSMC=-1
    cumsumRef=cumsum(Ref_SinfHistogram.weights)
    cumsumIS=cumsum(IS_SinfHistogram.weights)
    cumsumSMC=cumsum(SMC_SinfHistogram.weights)
    KolmogorovSmirnovIS=maximum(abs.(cumsumIS.-cumsumRef))
    KolmogorovSmirnovSMC=maximum(abs.(cumsumSMC.-cumsumRef))
    println("---------> Kolmogorov Smirnov distance between true posterior and IS/RSMC estimates = ",KolmogorovSmirnovIS," / ",KolmogorovSmirnovSMC)
end

println("\n*********************** End of Bart run ***********************\n ")



