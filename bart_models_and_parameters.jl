#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

################################################## Ref distribution & proposal kernel ################################################## 

mutable struct OrsteinUhlenbeckParams

    #parameters    
    alpha_prop::Float64 # stdev of O-Uhlenbeck
    pstar::Float64 # target acceptance probability
    c::Float64 # coefficient for stdev O-Uhlenbeck adaptation
    nMove::Int64 # number of kernel application
     # constructeur
    function OrsteinUhlenbeckParams(a_,p_,c_,n_)
        new(a_,p_,c_,n_)
    end
    
end

# reference distribution
abstract type AbstractRefDistrib end 

mutable struct logNormal <: AbstractRefDistrib

    #parameters ref law
    name::String
    paramDim::Int64 # dimension
    mu::Float64  
    sigma::Float64
    sigma_ref::Float64
    mu_ref::Float64

     # constructor
    function logNormal(p,m,s)
        new("logNormalLaw",p,m,s,sqrt(log(s^2/m^2 + 1)),log(m^2 / sqrt(s^2+m^2)))
    end
    
end

# lognormal sampler 
function refSampler(refDistrib::logNormal,N_samples,rng)

    return exp.(refDistrib.mu_ref .+ refDistrib.sigma_ref .* randn(rng,Float64,(N_samples,refDistrib.paramDim)))
    
end

################################################## FOMs ################################################## 

abstract type AbstractFOM end 

#1D analytical model

mutable struct Fom1D <: AbstractFOM

    L::Float64
    p1::Float64
    p2::Float64
    p3::Float64
    obs::Vector{Float64}
    varObsNoise::Float64
    nbFOMcalls::Int64

    # constructeurs par défaut
    function Fom1D(L_,p1_,p2_,p3_,obs_,varObsNoise_)
        new(L_,p1_,p2_,p3_,obs_,varObsNoise_,0)
    end
    
    
end


# 2D thermal block PDE model

mutable struct PDE_tbp <: AbstractFOM

    fom::PyObject
    fom_data::Dict{Any, Any}
    sizeSolution::Int64
    obs_point::Vector{Int64}
    obs::Vector{Float64}
    varObsNoise::Float64
    nbFOMcalls::Int64

    # constructor
    function PDE_tbp(p1_,p2_,diameter_,obs_point_,obs_,varObsNoise_)
        tbp = pm_basic.thermal_block_problem(num_blocks=(p1_,p2_),parameter_range=(0.001, 1))
        fom_, fom_data_ = pm_basic.discretize_stationary_cg(tbp, diameter = diameter_)
        sizeSolution_=length(fom_.solve(ones(p1_*p2_)).to_numpy())
        new(fom_, fom_data_,sizeSolution_,obs_point_,obs_,varObsNoise_,0)
    end 
    
end
################################################## ROMs ################################################## 

abstract type AbstractROM end 

#1D analytical spline model

mutable struct SplineApprox <: AbstractROM
    
    fomSpline::Fom1D
    xSample::AbstractVector{Float64}
    fFom::AbstractVector{Float64}
    fRom::BSplineManifold
    nbROMcalls::Int64

    # constructors 
    function SplineApprox(fomSpline_::Fom1D,xs_::AbstractVector{Float64},fs_::AbstractVector{Float64})
        p=sortperm(xs_)
        xs=xs_[p]
        fs=fs_[p]
        new(fomSpline_,xs,fs,interpolate(xs,fs),0)
    end

end

function interpolate(xs::AbstractVector, fs::AbstractVector{T}) where T
    # Cubic open B-spline space
    p = 3
    k = KnotVector(xs) + KnotVector([xs[1],xs[end]]) * p
    P = BSplineSpace{p}(k)

    # dimensions
    m = length(xs)
    n = dim(P)

    # The interpolant function has a f''=0 property at bounds.
    ddP = BSplineDerivativeSpace{2}(P)
    dda = [bsplinebasis(ddP,j,xs[1]) for j in 1:n]
    ddb = [bsplinebasis(ddP,j,xs[m]) for j in 1:n]

    # Compute the interpolant function (1-dim B-spline manifold)
    M = [bsplinebasis(P,j,xs[i]) for i in 1:m, j in 1:n]
    M = vcat(dda', M, ddb')
    y = vcat(zero(T), fs, zero(T))
    return BSplineManifold(pinv(M,1e-1*eps(eltype(M)))*y, P)
end

function updateROM!(spline::SplineApprox,newSample)

    snapshot,newFFom=evaluateFOM(fom,newSample) 
    println("sample / snapshot / snapshotPhi : ",newSample,"/",snapshot,"/",newFFom)
    xs=vcat(spline.xSample, newSample)[:,1]
    fs=vcat(spline.fFom, newFFom)[:,1]
    p=sortperm(xs)
    spline.xSample=xs[p]
    spline.fFom=fs[p]
    spline.fRom=interpolate(xs[p],fs[p])
    return true
    
end


# 2D thermal block PDE RB model

mutable struct RBApprox <: AbstractROM
    
    fomTbp::Ref{PDE_tbp}
    rom::PyObject
    xSample::Matrix{Float64}
    reductor::PyObject
    nbROMcalls::Int64

    # constructeurs 
    function RBApprox(fomTbp_::PDE_tbp,x_training::Matrix{Float64},N_samplesInit_)

        training_set = []
        greedy_data = []
        for k=1:length(x_training[:,1])
            push!(training_set,Dict("diffusion" =>  PyObject(x_training[k,:])))
        end 
        reductor_ = pm_basic.CoerciveRBReductor(fomTbp_.fom,
                                                product=fomTbp_.fom.h1_0_semi_product,
                                                coercivity_estimator=pm_basic.ExpressionParameterFunctional("min(diffusion)", fomTbp_.fom.parameters))
        @suppress begin 
            greedy_data =  pm_basic.rb_greedy(fomTbp_.fom, reductor_, training_set, max_extensions=N_samplesInit_) 
        end
        rom_ = greedy_data["rom"]
        xs_=zeros(N_samplesInit_,length(x_training[1,:]))
        for k=1:N_samplesInit_
            xs_[k,:]=greedy_data["max_err_mus"][k]["diffusion"]
        end
        println("Error bounds begining/end greedy algorithm : ",greedy_data["max_errs"][1]," / ",greedy_data["max_errs"][N_samplesInit_])
        new(Ref(fomTbp_),rom_,xs_,reductor_,0)
    end
    function RBApprox(fomTbp::PDE_tbp,rom::PyObject, xSample::Matrix{Float64},reductor::PyObject,nbROMcalls::Int64)
       new(Ref(fomTbp),rom,xSample,reductor,nbROMcalls)
    end
    function RBApprox()
        new()
    end

end

function updateROM!(RB::RBApprox,newSample)
    
    println("sample  : ",newSample)
    training_set = []
    greedy_data = []
    push!(training_set,Dict("diffusion" =>  PyObject(newSample)))
    @suppress begin 
        greedy_data =  pm_basic.rb_greedy(RB.fomTbp[].fom, RB.reductor, training_set, max_extensions=1) 
    end
    println("Error bound before ROM update : ",greedy_data["max_errs"][1])
    RB.rom = greedy_data["rom"]
    RB.xSample=[RB.xSample;newSample]
    if greedy_data["max_errs"][1]<=1e-10
        continueUpdate=false
    else
        continueUpdate=true
    end
    RB.fomTbp[].nbFOMcalls=RB.fomTbp[].nbFOMcalls+1
    return continueUpdate
    
end


################################################## FOMs & ROMs evaluation ################################################## 


#evaluate 1D analytical model FOM and ROM & error

function evaluateSFOM(fom::Fom1D,sample)
   
    m = length(sample)
    y = [1/sample[i]  for i in 1:m ] 
    ztmp=y.>fom.L
    y=@. y*(1-ztmp)+ztmp*fom.L;

    ztmp1=sample.<fom.p1
    mask=[ztmp1[i] && sample[i]>fom.p2 for i in 1:m ];
    y=@. ztmp1*y+mask*(sin(sample-fom.p2)*sin(sample-fom.p2))*fom.p3+(1-ztmp1)*(sin(fom.p1-fom.p2)*sin(fom.p1-fom.p2)-(sample-fom.p1)*0.1)*fom.p3;
    
    fom.nbFOMcalls=fom.nbFOMcalls+m
    s=max.(y,0)

    return 1 .-max.(fom.L.-s,0)./fom.L

end

function evaluateFOM(fom::Fom1D,sample)
    
    S=evaluateSFOM(fom,sample)
    norm2PhiObs=norm(fom.obs)^2

    return ( norm2PhiObs .- (S.-fom.obs).^2) ./ fom.varObsNoise, S

end

function evaluateSROM(spline::SplineApprox,sample)

    m = length(sample)
    s=min.(max.([unbounded_mapping(spline.fRom,sample[i])  for i in 1:m],0.),1.5*fom.L).*Int64.(sample.<30)

    spline.nbROMcalls=spline.nbROMcalls+m

    return s

end

function evaluateSROMandSError(spline::SplineApprox,sample)

    m = length(sample)
    s=min.(max.([unbounded_mapping(spline.fRom,sample[i])  for i in 1:m],0.),1.5*fom.L).*Int64.(sample.<30)
    e=(abs.(s-evaluateSFOM(fom,sample).*Int64.(sample.<30)))[:,1]
    fom.nbFOMcalls=fom.nbFOMcalls-m # in real situation,  error computed withour resorting to fom !
   
    spline.nbROMcalls=spline.nbROMcalls+m

    return s,e

end

function evaluateROM(spline::SplineApprox,sample)

    return evaluateROMandSROM(spline,sample)[1]

end

function evaluateROMandSROM(spline::SplineApprox,sample)

    SROM=evaluateSROM(spline,sample)
    norm2PhiObs=norm(fom.obs)^2

    return ( norm2PhiObs .- (SROM.-fom.obs).^2) ./ fom.varObsNoise, SROM

end

function evaluateROMandError(spline::SplineApprox,sample)

    SROM,SError=evaluateSROMandSError(spline,sample)
    norm2PhiObs=norm(fom.obs)^2
    V=( norm2PhiObs .- (SROM.-fom.obs).^2) ./ fom.varObsNoise
    E=( SError.^2 .+ 2 .* abs.(SROM.-fom.obs))  ./ fom.varObsNoise
    #E=( (SError.+fom.obs).^2 .- norm2PhiObs) ./ fom.varObsNoise

    return V,E,E

end

#evaluate 2D thermal block PDE model FOM and ROM & error

function evaluateSFOM(fomTbp::PDE_tbp,samples)
   
        m = length(samples[:,1])
        n=length(fomTbp.obs_point)
        y=zeros(m,n)
        y_max=zeros(m)
        @suppress begin
                for i_snap=1:m
                    solPDE=fomTbp.fom.solve(samples[i_snap,:]).to_numpy()
                    y[i_snap,:]=solPDE[fomTbp.obs_point] 
                    y_max[i_snap]=maximum(solPDE)
                end
        end
        fomTbp.nbFOMcalls=fomTbp.nbFOMcalls+m
        
        return  y, y_max # 1 .-max.(fomTbp.L.-y_max,0)./fomTbp.L 
end
    
function evaluateFOM(fomTbp::PDE_tbp,samples)

    sFomObsPoint,sFomInf=evaluateSFOM(fomTbp,samples) 
    V=zeros(length(samples[:,1]))
    for i_obs=1:length(fomTbp.obs_point)
        V.= V .+ ((fomTbp.obs[i_obs]).^2 .- (sFomObsPoint[:,i_obs].-fomTbp.obs[i_obs]).^2) ./ fomTbp.varObsNoise
    end
    return V, sFomInf

end

function evaluateSROM(RBRom::RBApprox,samples)

    m = length(samples[:,1])
    n=length(RBRom.fomTbp[].obs_point)
    y=zeros(m,n)
    y_max=zeros(m)
    for i_snap=1:m
        solPDE=RBRom.reductor.reconstruct(RBRom.rom.solve(samples[i_snap,:])).to_numpy()
        y[i_snap,:]=solPDE[RBRom.fomTbp[].obs_point] 
        y_max[i_snap]=maximum(solPDE)
        end
    RBRom.nbROMcalls=RBRom.nbROMcalls+m
    return y, y_max 
    

end

function evaluateSROMandSError(RBRom::RBApprox,samples)
    
    m = length(samples[:,1])
    y=zeros(m,length(RBRom.fomTbp[].obs_point))
    y_max=zeros(m)
    e=zeros(m)
    for i_snap=1:m
        solPDE=RBRom.reductor.reconstruct(RBRom.rom.solve(samples[i_snap,:])).to_numpy()
        y[i_snap,:]=solPDE[RBRom.fomTbp[].obs_point] 
        y_max[i_snap]=maximum(solPDE)
        e[i_snap]=RBRom.rom.estimate_error(samples[i_snap,:])[1]
    end   
    RBRom.nbROMcalls=RBRom.nbROMcalls+m
    

    return y,e,y_max 
end


function evaluateROM(RBRom::RBApprox,samples)

    return evaluateROMandSROM(RBRom,samples)[1]

end

function evaluateROMandSROM(RBRom::RBApprox,samples)

    sRomObsPoint,sRomInf=evaluateSROM(RBRom,samples)
    VRom=zeros(length(samples[:,1]))
    for i_obs=1:length(RBRom.fomTbp[].obs_point)
        VRom.= VRom .+ ((RBRom.fomTbp[].obs[i_obs]).^2 .- (sRomObsPoint[:,i_obs].-RBRom.fomTbp[].obs[i_obs]).^2) 
    end
    return VRom./ RBRom.fomTbp[].varObsNoise, sRomInf

end


function evaluateROMandError(RBRom::RBApprox,samples)

    sRomObsPoint,eRom=evaluateSROMandSError(RBRom,samples)
    n=length(samples[:,1])
    VRom=zeros(n)
    ERom=zeros(n)
    for i_obs=1:length(RBRom.fomTbp[].obs_point)
        VRom.= VRom .+ ( (RBRom.fomTbp[].obs[i_obs]).^2 .- (sRomObsPoint[:,i_obs].-RBRom.fomTbp[].obs[i_obs]).^2) 
        ERom.= ERom .+ (  eRom.*(eRom .+ 2 .*abs.(sRomObsPoint[:,i_obs].-RBRom.fomTbp[].obs[i_obs])) ) 
    end
    VRom.=VRom./ RBRom.fomTbp[].varObsNoise
    ERom.=ERom ./ RBRom.fomTbp[].varObsNoise
    return VRom,ERom,ERom 

end


################################################## BART parameters ################################################## 

mutable struct BART_parameters
 
    bridge::Bool
    budget::Int64
    N_samples::Int64
    proportionKilling::Float64
    logCost::Float64
    miniNumberHitsForIS::Int64
    epsilon::Float64
    refLaw::AbstractRefDistrib
    alphaProp_init::Float64
    kernelParam::OrsteinUhlenbeckParams
    beta_max::Float64
    rng
    fom::AbstractFOM

    function BART_parameters(fom_,bridge_,budget_,N_samples_,proportionKilling_,logCost_,miniNumberHitsForIS_,refLaw_,alphaProp_init_,kernelParam_,beta_max_,rng_)
        new(bridge_,budget_,N_samples_,proportionKilling_,logCost_,miniNumberHitsForIS_,1,refLaw_,alphaProp_init_,kernelParam_,beta_max_,rng_,fom_)
    end
end

function saveInputParameters(filePath,param,rom_inital)
    open(filePath, "w") do io
      println(io," BART parameters= ",param)
      println(io," rom initial=", rom_inital)
    end
end


