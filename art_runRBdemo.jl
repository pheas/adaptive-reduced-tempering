#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

#####################################################################################
#####################################################################################
#       This demo implements rare event simulation,
#       where the score function is the L_p norm of a parametric PDE solution approximated by reduced basis. 
#       The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
#       The PDE is parametrized by a random vector of diffusion coefficients.
#       The reference distribution for the random vector components are independent log-normal distributions. 
#       Details on the model can be found in in the paper  
#      `Adaptive reduced tempering For Bayesian inverse problems and rare event simulation' by F. Cérou, P. Héas and M. Rousset.   
#####################################################################################
#####################################################################################


include("art_librairies.jl")
include("art_main_algorithm.jl")
include("art_models_and_parameters.jl")
include("art_smc_algorithms.jl")
include("art_plots.jl")

println("\n************************************************************************************************** ")
println("*********************** Adaptive reduced tempering for rare event simulation demo ***************** ")
println("****************************************************************************************************\n ")

#####################################################################################
#----------------------------- Parameter setting -----------------------------------#
#####################################################################################

#################
## output path
#################

rootpath="./test/"
extpath="art_RBdemo"

#################
## ART parameters
#################
# true if bridging ART simulations
bridge=true
# budget K 
budget=200
# number of snapshots to build initial reduced score
N_samplesInit=5
# number of particles N
N_samples=100
# theta (=1-exp(Ent(mu_new|mu_current)))
proportionKilling=0.01
#worst logcost threshold 
logCost=0.001 
#number of hits before Importance Sampling trigerred  
miniNumberHitsForIS=2
# if stop reduced score update if sufficiently accurate (epsilon = 1-mean(Srom) else (epsilon <0)
epsilon=0.05
#Markovian kernel 
alphaProp_init=0.3
kernelParam=OrsteinUhlenbeckParams(alphaProp_init,0.5,0.015,30)
#Type of L_p norm for the score  
meanScore=1 # 1 if temperature mean (p=1) or !=1 if temperature max (p=infinity) 
#Score level to reach
L_max=0.5
#Maximum inverse temperature (truncating the infinity value)
beta_max=50

#################
## PDE and RB parameters 
#################
nbBlocksX=2
nbBlocksY=2
diameter=0.02
dimParameter=nbBlocksX*nbBlocksY

#################
#ref distribution parameters
#################
seed=1233
rng = MersenneTwister(seed)
refLaw=logNormal(dimParameter,0.6,0.8)

#####################################################################################
#----------------------------- Initialization --------------------------------------#
#####################################################################################
println("\n*********************** Initialization ***********************\n ")
println("---------> Building thermal block FOM ...")
fom=@suppress begin  PDE_tbp(nbBlocksX,nbBlocksY,diameter,L_max,meanScore) end 
println("FOM system of dimension = ",fom.sizeSolution)

println("---------> Building thermal block initial RB ...")
initSamples=refSampler(refLaw,N_samplesInit*10,rng)
rom_inital=@suppress begin   RBApprox(fom,initSamples,N_samplesInit) end

#####################################################################################
#----------------------------- Running ART    --------------------------------------#
#####################################################################################
println("\n*********************** Running ART ***********************\n ")
println("---------> Lauching ART ...")
#creating ART param object and saving it
param=ART_parameters(fom,bridge,budget,N_samples,proportionKilling,logCost,miniNumberHitsForIS,epsilon,refLaw,alphaProp_init,kernelParam,meanScore,L_max,beta_max,rng)
saveInputParameters(rootpath*extpath*"_parameters.txt",param,rom_inital)
#running ART 
timing = @elapsed p_IS,p_SMC,p_IS_pointwise,p_SMC_pointwise, nbROMcalls, nbFOMcalls=ART(rom_inital,param)

#####################################################################################
#----------------------------- Results saving and display   ------------------------#
#####################################################################################
println("\n*********************** Result saving ***********************\n ")
#saving ART outputs
println("saving output data ---------> ",rootpath*extpath*"_outputs.jld")
save(rootpath*extpath*"_outputs.jld", "p_IS_pointwise", p_IS_pointwise , "p_IS", p_IS, "p_SMC", p_SMC,"p_SMC_pointwise", p_SMC_pointwise , "nbROMcalls", nbROMcalls, "nbFOMcalls", nbFOMcalls, "timing", timing)

# results visualization
p_True=0.0006072672165436441
RomGain=4.5e-2
p00,p01=displayResults(p_True,p_IS,p_SMC,budget,nbROMcalls,nbFOMcalls,RomGain,1)
println("saving plot ---------> ",rootpath*extpath*"_mean_wrt_iteration.pdf")
savefig(p00,rootpath*extpath*"_mean_wrt_iteration.pdf") 
println("saving plot ---------> ",rootpath*extpath*"_RMSEvsCost_wrt_iteration.pdf")
savefig(p01,rootpath*extpath*"_RMSEvsCost_wrt_iteration.pdf") 

println("\n*********************** End of ART run ***********************\n ")







  
  