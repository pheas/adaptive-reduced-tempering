#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

## saving fom and RB objects to format readeable using python scripts (see pymor tutorial)

 function savePyObject_0(object,it,visupath)

    if (typeof(object)==PDE_tbp)
        f = pybuiltin("open")(string(visupath,"fom.pickle"),"wb")
        pyObject=object.fom
        pickle.dump(pyObject,f)
        f.close()
    elseif (typeof(object)==RBApprox)
        f = pybuiltin("open")(string(visupath,"rom",string(it),".pickle"),"wb")
        pyObject=object.rom
        pickle.dump(pyObject,f)
        f.close()
        f = pybuiltin("open")(string(visupath,"romRed",string(it),".pickle"),"wb")
        pyObject=object.reductor
        pickle.dump(pyObject,f)
        f.close()
    end
    
    
end
function saveFOM(fom,visupath)
    savePyObject_0(fom,1,visupath)
end
function saveRB(rom,visupath,it)
    savePyObject_0(rom,it,visupath)
end

## plots of estimates
 
function displayResults(p_True,p_IS,p_SMC,budget,nbROMcalls,nbFOMcalls,costRomEval,costFomEval)
    
k=1
while p_IS[k]==0 && k<budget
    k+=1
end
k=k-3
absIt=1:budget
#mean IS and SMC plot
p00=plot(absIt,[p_IS,ones(budget)*p_True,p_SMC],label=["IS" "truth" "RSMC"],legend=:topleft, ylims=(0.,p_True*2),xlabel = "number of snapshots", ylabel =L"$\hat p$")  
p01=plot( nbROMcalls[k:end].*costRomEval .+ nbFOMcalls[k:end].*costFomEval,((p_SMC[k:end].-p_True).^2)./(p_True^2),label=" IS",ylabel = L"${{\mathbb{E}[(\hat p -p^\star)^2]}}/{{p^\star}^2}$", xlabel = "cost")
p01=plot!(nbROMcalls[k:end].*costRomEval .+ nbFOMcalls[k:end].*costFomEval,((p_IS[k:end].-p_True).^2)./(p_True^2), label=" RSMC",legend=:bottomleft,yaxis=:log,ylims=(1e-4,1))

display(plot(p00,p01,layout=(2,1)))

return [p00,p01]
 
end
