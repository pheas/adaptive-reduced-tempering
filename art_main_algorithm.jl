
#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

# Main algorithm: adaptive reduced tempering (ART)

function ART(romInit,param)

    N_samples=param.N_samples
    budget=param.budget
    logCost=param.logCost
    miniNumberHitsForIS=param.miniNumberHitsForIS

    #############
    #Initialization
    #############
     
    samples=refSampler(param.refLaw,N_samples,param.rng)
    Z=[[1.]]
    M=[[0.]]
    push!(M[1],0.)
    beta=[[0.]]
    w=[ones(N_samples)./N_samples]
    roms=[]
    samplesHistory=[copy(samples)]
    push!(Z,[1.])
    push!(M,[0.])
    push!(beta,[0.])
    push!(w,w[1])
    param.kernelParam.alpha_prop=param.alphaProp_init
    push!(roms,romInit); push!(roms,deepcopy(romInit))
    maxSampling=true
    NumberHits=0
    FirstIndexForIS=-1
    IS_estimates_pointwise=zeros(budget)
    IS_estimate=zeros(budget)
    IS_validPoints=ones(budget)
    SMC_estimates_pointwise=zeros(budget)
    SMC_estimate=zeros(budget)
    nbROMeval=zeros(budget)
    nbFOMeval=zeros(budget)
    param.fom.nbFOMcalls=0
    
    #############
    # SMC Loop
    #############
    logCostLevel=logCost
    continueROMUpdate=true
    ROMconvergence=0

    ##### untill the snapshot budget is reached or ROM sufficiently accurate ##### 
    it=1
    maxNumberRomConvergenceCriterion=2  # max number of times the stopping criterion for ROM convergence has to be met
    maxHistory=30  # maximum number of back steps in sample/rom history

    while (it<budget) &&  (continueROMUpdate)
        it=it+1
        println("#################### ART iteration ",it-1," ####################")
        if ROMconvergence>maxNumberRomConvergenceCriterion  # to exit while loop at the next iteration 
            continueROMUpdate=false
        end 
        # adaptive increase of levels (cte normalization factor & entropic criterion)
        sRom,eRom,conv,eRomNoTreshold= SMC_increase_beta!(roms[it],samples,w[it],Z[it],M[it],beta[it],logCostLevel,param)
        if (conv) ROMconvergence=ROMconvergence+1 end
        push!(samplesHistory,copy(samples))
        push!(w,w[it])
        push!(beta,[beta[it][end]])
        push!(Z,[Z[it][end]])
        println("Z/beta=",Z[it][end],"/",beta[it][end]);
      
       # sampling with learning function (multinomial sampling & kernel mixing or max error)
        if (!maxSampling)
            items = [i for i in 1:N_samples]'
            weights = Weights(w[it])
            index=wsample(param.rng,items, weights,1)
            snapshotSample=samples[index,:]
            mutate!(roms[it],snapshotSample,beta[it][end],param)
        else
            snapshotSample=samples[sortperm(eRomNoTreshold)[N_samples],:]';
        end
        
       # updating the ROM 
       push!(roms,deepcopy(roms[it]))
       snapshot=evaluateFOM(param.fom,snapshotSample) 
       println("sample / snapshot: ",snapshotSample,"/",snapshot)
       #println("snapshot: ",snapshot)
       if continueROMUpdate 
            updateROM!(roms[it+1],snapshotSample,snapshot)
       else
            println("-------------------------------------------- end of ROM updates ...")
       end
       if (it-1-max(maxHistory,maxHistory)>0) # releaving unecessary memory 
            roms[it-1-max(maxHistory,maxHistory)]=RBApprox() 
            GC.gc(); 
       end

       # learning function update & setting first index for IS estimator 
       if (snapshot[end]>=1)
            NumberHits+=1
            println("Number of hits=",NumberHits)
            if (NumberHits> miniNumberHitsForIS && FirstIndexForIS==-1)
                println("-------------------------------------------- swithcing learning function to random sampling ... ")
                maxSampling=false
                FirstIndexForIS=it  
            end
       end 
        
       # importance sampling estimate at snapshot sample (performed only if miniNumberHitsForIS reached) & SMC estimate
        SMC_estimates_pointwise[it]=Z[it+1][end]*sum(w[it+1].*exp.(-beta[it+1][end].*sRom.+sum(M[it])).*Float64.(sRom .>=1)) 
        if  (FirstIndexForIS>0)
           sRomSnapshot=evaluateROM(roms[it],snapshotSample)[1]
           IS_estimates_pointwise[it]=Z[it+1][end]*exp(-beta[it+1][end]*sRomSnapshot.+sum(M[it]))*Float64(snapshot[end] >= 1)
           println("IS point estimate = ", IS_estimates_pointwise[it], ", SMC point estimate = ",SMC_estimates_pointwise[it])
           if (!maxSampling && logCostLevel <Inf64 )
                logCostLevel=Inf64
                println("-------------------------------------------- end of entropic criterion ...  ")
           end
        else
           IS_validPoints[it]=0
           println("SMC point estimate = ",SMC_estimates_pointwise[it])
        end

       # bridge SMCs or decide to restart from the beginning the next SMC simulation 
       if bridge
        if continueROMUpdate
            # guaranteeing entropic criterion
            bridging!(Z,M,beta,w,samplesHistory,samples,sRom,eRom,it,roms,logCostLevel,maxHistory,param)
        end
       else
        if continueROMUpdate
            # restart simulation 
            samples.=refSampler(refLaw,N_samples,param.rng)
            push!(M,[0.])
            w[it+1]=w[1]
            Z[it+1][end]=1.
            beta[it+1][end]=0.
            param.kernelParam.alpha_prop=param.alphaProp_init
        end
       end

       nbROMeval[it]=roms[it].nbROMcalls
       nbFOMeval[it]=param.fom.nbFOMcalls
    end
    
    #####  IS for remaining snapshot budget  ##### 
    for itLeft=it+1:budget
        snapshotSample=samples[ceil.(Int64,rand(param.rng,1)*N_samples),:]
        snapshot=evaluateFOM(param.fom,snapshotSample) 
        sRomSnapshot=evaluateROM(roms[it],snapshotSample)[1]
        IS_estimates_pointwise[itLeft]=Z[it+1][end]*exp(-beta[it+1][end]*sRomSnapshot+sum(M[it]))*Float64(snapshot[end] >= 1)
        SMC_estimates_pointwise[itLeft]=SMC_estimates_pointwise[it]
        nbROMeval[itLeft]=roms[it].nbROMcalls
        nbFOMeval[itLeft]=param.fom.nbFOMcalls
  
    end     

    ##### Prepare IS estimates outputs ##### 

    #gather IS and SMC estimates from FirstIndexForIS 
    if ((FirstIndexForIS>0 && FirstIndexForIS+1<=length(IS_estimate))) 
        IS_estimate[FirstIndexForIS+1:length(IS_estimate)].=[sum(IS_estimates_pointwise[FirstIndexForIS+1:k])/sum(IS_validPoints[FirstIndexForIS+1:k])  for k in FirstIndexForIS+1:length(IS_estimate)]
        SMC_estimate[FirstIndexForIS+1:length(IS_estimate)].=[sum(SMC_estimates_pointwise[FirstIndexForIS+1:k])/sum(IS_validPoints[FirstIndexForIS+1:k])  for k in FirstIndexForIS+1:length(SMC_estimate)]
    end
    # exclude case where IS_estimate undefined (points from FirstIndexForIS+1 not valid points)
    for k in eachindex(IS_estimate)  
        if isnan(IS_estimate[k])
            IS_estimate[k]=0
        end
    end
    println("***** End run: IS estimate = ", IS_estimate[end], ", SMC estimate = ",SMC_estimate[end]," *****")
   return  IS_estimate, SMC_estimate,IS_estimates_pointwise, SMC_estimates_pointwise,nbROMeval, nbFOMeval

end     