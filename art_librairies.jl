#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

using JLD
using Plots
using Random
using StatsBase
using SpecialFunctions
using StaticArrays
using Statistics
using LinearAlgebra
using LaTeXStrings
using BasicBSpline
using PyCall
using SharedArrays
using Suppressor
using Roots
using JuMP, Ipopt
using Permutations

pm_basic = pyimport_conda("pymor.basic", "pymor")
pickle = pyimport("pickle")
